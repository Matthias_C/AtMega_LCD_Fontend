//
// Created by matthiasclemen on 27.03.18.
//

extern "C" {
#include <stdlib.h>
#include <inttypes.h>
#include "twi.h"
}

#include "Wire.h"

uint8_t Wire::rxBuffer[BUFFER_LENGTH];
uint8_t Wire::rxBufferIndex = 0;
uint8_t Wire::rxBufferLength = 0;

uint8_t Wire::txAddress = 0;
uint8_t Wire::txBuffer[BUFFER_LENGTH];
uint8_t Wire::txBufferIndex = 0;
uint8_t Wire::txBufferLength = 0;

uint8_t Wire::transmitting = 0;
void (*Wire::user_onRequest)(void);
void (*Wire::user_onReceive)(int);

uint8_t Wire::endTransmission() {
  return endTransmission(true);
}

// sets function called on slave write
void Wire::onReceive(void (*function)(int)) {
  user_onReceive = function;
}

// sets function called on slave read
void Wire::onRequest(void (*function)(void)) {
  user_onRequest = function;
}

uint8_t Wire::requestFrom(uint8_t address, uint8_t quantity, uint8_t sendStop) {
  // clamp to buffer length
  if (quantity > BUFFER_LENGTH) {
    quantity = BUFFER_LENGTH;
  }
  // perform blocking read into buffer
  uint8_t read = twi_readFrom(address, rxBuffer, quantity, sendStop);
  // set rx buffer iterator vars
  rxBufferIndex = 0;
  rxBufferLength = read;

  return read;
}
uint8_t Wire::requestFrom(uint8_t address, uint8_t quantity) {
  return requestFrom((uint8_t) address, (uint8_t) quantity, (uint8_t) true);
}

void Wire::begin(void) {
  rxBufferIndex = 0;
  rxBufferLength = 0;

  txBufferIndex = 0;
  txBufferLength = 0;

  twi_init();
}

void Wire::beginTransmission(uint8_t address) {
  // indicate that we are transmitting
  transmitting = 1;
  // set address of targeted slave
  txAddress = address;
  // reset tx buffer iterator vars
  txBufferIndex = 0;
  txBufferLength = 0;
}

uint8_t Wire::endTransmission(uint8_t sendStop) {
  // transmit buffer (blocking)
  uint8_t ret = twi_writeTo(txAddress, txBuffer, txBufferLength, 1, sendStop);
  // reset tx buffer iterator vars
  txBufferIndex = 0;
  txBufferLength = 0;
  // indicate that we are done transmitting
  transmitting = 0;
  return ret;
}

Wire::Wire() {
  // Empty
}
void Wire::begin(uint8_t address) {
  twi_setAddress(address);
  twi_attachSlaveTxEvent(onRequestService);
  twi_attachSlaveRxEvent(onReceiveService);
  begin();
}
size_t Wire::write(const uint8_t *data, size_t quantity) {
  if (transmitting) {
    // in master transmitter mode
    for (size_t i = 0; i < quantity; ++i) {
      write(data[i]);
    }
  } else {
    // in slave send mode
    // reply to master
    twi_transmit(data, quantity);
  }
  return quantity;
}
size_t Wire::write(uint8_t data) {
  if (transmitting) {
    // in master transmitter mode
    // don't bother if buffer is full
    if (txBufferLength >= BUFFER_LENGTH) {
      // TODO implemnt
      //setWriteError();
      return 0;
    }
    // put byte in tx buffer
    txBuffer[txBufferIndex] = data;
    ++txBufferIndex;
    // update amount in buffer
    txBufferLength = txBufferIndex;
  } else {
    // in slave send mode
    // reply to master
    twi_transmit(&data, 1);
  }
  return 1;
}

// must be called in:
// slave rx event callback
// or after requestFrom(address, numBytes)
int Wire::available(void) {
  return rxBufferLength - rxBufferIndex;
}

// must be called in:
// slave rx event callback
// or after requestFrom(address, numBytes)
uint8_t Wire::read(void) {
  uint8_t value = -1;

  // get each successive byte on each call
  if (rxBufferIndex < rxBufferLength) {
    value = rxBuffer[rxBufferIndex];
    ++rxBufferIndex;
  }

  return value;
}

// behind the scenes function that is called when data is requested
void Wire::onRequestService(void) {
  // don't bother if user hasn't registered a callback
  if (!user_onRequest) {
    return;
  }
  // reset tx buffer iterator vars
  // !!! this will kill any pending pre-master sendTo() activity
  txBufferIndex = 0;
  txBufferLength = 0;
  // alert user program
  user_onRequest();
}

// behind the scenes function that is called when data is received
void Wire::onReceiveService(uint8_t *inBytes, int numBytes) {
  // don't bother if user hasn't registered a callback
  if (!user_onReceive) {
    return;
  }
  // don't bother if rx buffer is in use by a master requestFrom() op
  // i know this drops data, but it allows for slight stupidity
  // meaning, they may not have read all the master requestFrom() data yet
  if (rxBufferIndex < rxBufferLength) {
    return;
  }
  // copy twi rx buffer into local read buffer
  // this enables new reads to happen in parallel
  for (uint8_t i = 0; i < numBytes; ++i) {
    rxBuffer[i] = inBytes[i];
  }
  // set rx iterator vars
  rxBufferIndex = 0;
  rxBufferLength = numBytes;
  // alert user program
  user_onReceive(numBytes);
}
void Wire::end() {
  twi_disable();
}
void Wire::setClock(uint32_t clock) {

  twi_setFrequency(clock);
}



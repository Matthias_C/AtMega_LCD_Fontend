//
// Created by matthiasclemen on 15.04.18.
//

#ifndef ATMEGALCD_FRONEND_SETTINGS_H
#define ATMEGALCD_FRONEND_SETTINGS_H

class Settings {

 public:
  void saveSettings();
  void restoreSettings();
};

#endif //ATMEGALCD_FRONEND_SETTINGS_H

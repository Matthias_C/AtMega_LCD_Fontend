//
// Created by matthiasclemen on 15.04.18.
//

#include "Settings.h"
#include "EEPROM.h"
#include "../Globals.h"
void Settings::saveSettings() {

  lcd.clear();
  lcd.setCursor(3, 2);
  lcd.print("Save");
  EEPROM.put(0, eepromSettings);
  _delay_ms(500);
  lcd.clear();

}
void Settings::restoreSettings() {
  EEPROM.get(0, eepromSettings);

}

//
// Created by matthiasclemen on 24.01.17.
//

#include "Interrupt.h"

interrupt *interrupt::owner[] = {0};

void interrupt::handler1() {
  if (owner[0])
    owner[0]->ServiceRoutine();
}

//void interrupt::handler2() {
//  if (owner[1])
//    owner[1]->ServiceRoutine();
//}

void interrupt::handler3() {
  if (owner[2])
    owner[2]->ServiceRoutine();
}

void interrupt::handler4() {
  if (owner[3])
    owner[3]->ServiceRoutine();
}

void interrupt::handler5() {
  if (owner[4])
    owner[4]->ServiceRoutine();
}

void interrupt::handler6() {
  if (owner[5])
    owner[5]->ServiceRoutine();
}

void interrupt::handler7() {
  if (owner[6])
    owner[6]->ServiceRoutine();
}

void interrupt::handler8() {
  if (owner[7])
    owner[7]->ServiceRoutine();
}

void interrupt::handler9() {
  if (owner[8])
    owner[8]->ServiceRoutine();
}

void interrupt::handler10() {
  if (owner[9])
    owner[9]->ServiceRoutine();
}

void interrupt::handler11() {
  if (owner[10])
    owner[10]->ServiceRoutine();
}

void interrupt::handler12() {
  if (owner[11])
    owner[11]->ServiceRoutine();
}

void interrupt::handler13() {
  if (owner[12])
    owner[12]->ServiceRoutine();
}

void interrupt::handler14() {
  if (owner[13])
    owner[13]->ServiceRoutine();
}

void interrupt::handler15() {
  if (owner[14])
    owner[14]->ServiceRoutine();
}

//void interrupt::handler16() {
//  if (owner[15])
//    owner[15]->ServiceRoutine();
//}

void interrupt::handler17() {
  if (owner[16])
    owner[16]->ServiceRoutine();
}

void interrupt::handler18() {
  if (owner[17])
    owner[17]->ServiceRoutine();
}

void interrupt::handler19() {
  if (owner[18])
    owner[18]->ServiceRoutine();
}

void interrupt::handler20() {
  if (owner[19])
    owner[19]->ServiceRoutine();
}

void interrupt::handler21() {
  if (owner[20])
    owner[20]->ServiceRoutine();
}

void interrupt::handler22() {
  if (owner[21])
    owner[21]->ServiceRoutine();
}

void interrupt::handler23() {
  if (owner[22])
    owner[22]->ServiceRoutine();
}

//void interrupt::handler24() {
//  if (owner[23])
//    owner[23]->ServiceRoutine();
//}

void interrupt::handler25() {
  if (owner[24])
    owner[24]->ServiceRoutine();
}

void interrupt::handler26() {
  if (owner[25])
    owner[25]->ServiceRoutine();
}

void interrupt::record(int interruptNumber, interrupt *i) {
  owner[interruptNumber - 1] = i;
}
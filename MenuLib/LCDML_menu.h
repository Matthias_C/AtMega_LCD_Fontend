//
// Created by matthiasclemen on 09.04.18.
//

#ifndef ATMEGALCD_FRONEND_LCDML_MENU_H
#define ATMEGALCD_FRONEND_LCDML_MENU_H

#include "src/LCDMenuLib2_menu.h"
#include "src/LCDMenuLib2.h"

//extern LCDMenuLib2_menu LCDML_0;

extern LCDMenuLib2 LCDML;


#include "LCDML_control.h"
#include "LCDML_display_menu.h"

#include "LCDML_display_menuFunctions.h"

#define _LCDML_DISP_cols  16
#define _LCDML_DISP_rows  4

#define _LCDML_DISP_cfg_cursor                     0x7E   // cursor Symbol
#define _LCDML_DISP_cfg_scrollbar                  1      // enable a scrollbar



class LCDML_menu {
 private:

 public:
  void begin();

};

#endif //ATMEGALCD_FRONEND_LCDML_MENU_H

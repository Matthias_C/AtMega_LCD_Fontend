//
// Created by matthiasclemen on 09.04.18.
//

#ifndef ATMEGALCD_FRONEND_LCDML_DISPLAY_MENU_H
#define ATMEGALCD_FRONEND_LCDML_DISPLAY_MENU_H

void lcdml_menu_clear();
void lcdml_menu_display();

#endif //ATMEGALCD_FRONEND_LCDML_DISPLAY_MENU_H

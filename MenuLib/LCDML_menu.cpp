//
// Created by matthiasclemen on 09.04.18.
//

#include <stdio.h>
#include "LCDML_menu.h"

LCDMenuLib2_menu LCDML_0 = LCDMenuLib2_menu(255, 0, 0, NULL, NULL); // root menu element (do not change);
LCDMenuLib2
    LCDML(LCDML_0, _LCDML_DISP_rows, _LCDML_DISP_cols, lcdml_menu_display, lcdml_menu_clear, lcdml_menu_control);

boolean COND_hide()  // hide a menu element
{
  return false;  // hidden
}

//LCDML_addAdvanced(0, LCDML_0, 1, NULL, "Information"  , mFunc_information, 0, _LCDML_TYPE_default);
// LCDML_add    (id,    prev_layer,   new_num, lang_char_array ,    callback_function)
LCDML_add         (0, LCDML_0, 1, "Information", NULL);
// LCDMenuLib_add(id, prev_layer,     new_num, condetion,   lang_char_array, callback_function, parameter (0-255), menu function type  )
LCDML_addAdvanced         (1, LCDML_0_1, 1, NULL, "Sensor1", mFunc_SensorInformation, 0, _LCDML_TYPE_default);
LCDML_addAdvanced         (2, LCDML_0_1, 2, NULL, "Sensor2", mFunc_SensorInformation, 1, _LCDML_TYPE_default);
LCDML_addAdvanced         (3, LCDML_0_1, 3, NULL, "Sensor3", mFunc_SensorInformation, 2, _LCDML_TYPE_default);
LCDML_addAdvanced         (4, LCDML_0_1, 4, NULL, "Sensor4", mFunc_SensorInformation, 3, _LCDML_TYPE_default);
LCDML_add         (5, LCDML_0, 2, "Settings", NULL);
LCDML_add         (6, LCDML_0_2, 1, "Network", NULL);
LCDML_add         (7, LCDML_0_2, 2, "System", mFunc_information);
LCDML_add         (8, LCDML_0_2_1, 1, "IP", mFunc_setting_IP);
LCDML_add         (9, LCDML_0_2_1, 2, "Port", mFunc_setting_Port);



// Example for condetions (for example for a screensaver)
// 1. define a condetion as a function of a boolean type -> return false = not displayed, return true = displayed
// 2. set the function name as callback (remove the braces '()' it gives bad errors)
// LCDMenuLib_add(id, prev_layer,     new_num, condetion,   lang_char_array, callback_function, parameter (0-255), menu function type  )
LCDML_addAdvanced (10, LCDML_0, 7, COND_hide, "screensaver", mFunc_screensaver, 0, _LCDML_TYPE_default);

// ***TIP*** Try to update _LCDML_DISP_cnt when you add a menu elment.

// menu element count - last element id
// this value must be the same as the last menu element
#define _LCDML_DISP_cnt    10

LCDML_createMenu(_LCDML_DISP_cnt);

void lcdml_menu_clear() {
  lcd.clear();
  lcd.setCursor(0, 0);
}

void lcdml_menu_display() {
  // update content
  // ***************
  if (LCDML.DISP_checkMenuUpdate()) {
    // clear menu
    // ***************
    LCDML.DISP_clear();

    // decalration of some variables
    // ***************
    // content variable
    char content_text[_LCDML_DISP_cols];  // save the content text of every menu element
    // menu element object
    LCDMenuLib2_menu *tmp;
    // some limit values
    uint8_t i = LCDML.MENU_getScroll();
    uint8_t maxi = _LCDML_DISP_rows + i;
    uint8_t n = 0;

    // check if this element has children
    tmp = LCDML.MENU_getObj()->getChild(LCDML.MENU_getScroll());
    if (tmp) {
      // loop to display lines
      do {
        // check if a menu element has a condetion and if the condetion be true
        if (tmp->checkCondetion()) {
          // check the type off a menu element
          if (tmp->checkType_menu() != 0) {
            // display normal content
            LCDML_getContent(content_text, tmp->getID());
            lcd.setCursor(1, n);
            lcd.print(content_text);
          } else {
            if (tmp->checkType_dynParam()) {
              tmp->callback(n);
            }
          }
          // increment some values
          i++;
          n++;
        }
        // try to go to the next sibling and check the number of displayed rows
      } while (((tmp = tmp->getSibling(1)) != NULL) && (i < maxi));
    }
  }

  if (LCDML.DISP_checkMenuCursorUpdate()) {
    // init vars
    uint8_t n_max = (LCDML.MENU_getChilds() >= _LCDML_DISP_rows) ? _LCDML_DISP_rows : (LCDML.MENU_getChilds());
    uint8_t scrollbar_min = 0;
    uint8_t scrollbar_max = LCDML.MENU_getChilds();
    uint8_t scrollbar_cur_pos = LCDML.MENU_getCursorPosAbs();
    uint8_t scroll_pos = ((1. * n_max * _LCDML_DISP_rows) / (scrollbar_max - 1) * scrollbar_cur_pos);


    // display rows
    for (uint8_t n = 0; n < n_max; n++) {
      //set cursor
      lcd.setCursor(0, n);

      //set cursor char
      if (n == LCDML.MENU_getCursorPos()) {
        lcd.write(_LCDML_DISP_cfg_cursor);
      } else {
        lcd.write(' ');
      }

      // delete or reset scrollbar
      if (_LCDML_DISP_cfg_scrollbar == 1) {
        if (scrollbar_max > n_max) {
          lcd.setCursor((_LCDML_DISP_cols - 1), n);
          lcd.write((uint8_t) 0);
        } else {
          lcd.setCursor((_LCDML_DISP_cols - 1), n);
          lcd.write(' ');
        }
      }
    }

    // display scrollbar
    if (_LCDML_DISP_cfg_scrollbar == 1) {
      if (scrollbar_max > n_max) {
        //set scroll position
        if (scrollbar_cur_pos == scrollbar_min) {
          // min pos
          lcd.setCursor((_LCDML_DISP_cols - 1), 0);
          lcd.write((uint8_t) 1);
        } else if (scrollbar_cur_pos == (scrollbar_max - 1)) {
          // max pos
          lcd.setCursor((_LCDML_DISP_cols - 1), (n_max - 1));
          lcd.write((uint8_t) 4);
        } else {
          // between
          lcd.setCursor((_LCDML_DISP_cols - 1), scroll_pos / n_max);
          lcd.write((uint8_t) ((scroll_pos % n_max) + 1));
        }
      }
    }
  }

}

void LCDML_menu::begin() {

  const uint8_t scroll_bar[5][8] = {
      {0B10001, 0B10001, 0B10001, 0B10001, 0B10001, 0B10001, 0B10001, 0B10001}, // scrollbar top
      {0B11111, 0B11111, 0B10001, 0B10001, 0B10001, 0B10001, 0B10001, 0B10001}, // scroll state 1
      {0B10001, 0B10001, 0B11111, 0B11111, 0B10001, 0B10001, 0B10001, 0B10001}, // scroll state 2
      {0B10001, 0B10001, 0B10001, 0B10001, 0B11111, 0B11111, 0B10001, 0B10001}, // scroll state 3
      {0B10001, 0B10001, 0B10001, 0B10001, 0B10001, 0B10001, 0B11111, 0B11111}  // scrollbar bottom
  };

  lcd.createChar(0, (uint8_t *) scroll_bar[0]);
  lcd.createChar(1, (uint8_t *) scroll_bar[1]);
  lcd.createChar(2, (uint8_t *) scroll_bar[2]);
  lcd.createChar(3, (uint8_t *) scroll_bar[3]);
  lcd.createChar(4, (uint8_t *) scroll_bar[4]);

  // LCDMenuLib Setup
  LCDML_setup(_LCDML_DISP_cnt);

  // Some settings which can be used

  // Enable Menü Rollover
  LCDML.MENU_enRollover();
  // Enable Screensaver (screensaver menu function, time to activate in ms)
  LCDML.SCREEN_enable(mFunc_screensaver, 10000); // set to 10 secounds

  LCDML.OTHER_jumpToFunc(mFunc_screensaver); // the parameter is the function name
};

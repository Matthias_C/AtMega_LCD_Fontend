//
// Created by matthiasclemen on 09.04.18.
//

#include <stdio.h>
#include <stdlib.h>
#include "LCDML_display_menuFunctions.h"
#include "LCDML_menu.h"

uint8_t g_ip_selected_part = 0; // when this value comes from an eeprom, load it in setup
uint8_t g_velocity_counter = 0;
void mFunc_setting_IP(uint8_t param) {
  char buff[17] = "                ";
  if (LCDML.FUNC_setup())          // ****** SETUP *********
  {
    g_ip_selected_part = 0;

    // setup function
    lcd.setCursor(0, 0);
    lcd.print(("IP:"));
    lcd.setCursor(0, 1);

    for (int i = 0; i < 4; ++i) {
      itoa(eepromSettings.ip[i], buff, 10);
      lcd.print(buff);
      if (i != 3)
        lcd.write('.');
    }
    lcd.setCursor(0, 2);
    lcd.print(("Mac:"));
    lcd.setCursor(0, 3);
    for (int i = 0; i < 6; ++i) {
      itoa(eepromSettings.mac[i], buff, 16);
      lcd.print(buff);
    }

    LCDML.FUNC_setLoopInterval(500);
    lcd.cursor();
  }

  if (LCDML.FUNC_loop())           // ****** LOOP *********
  {

    if (LCDML.BT_checkUp()) {
      eepromSettings.ip[g_ip_selected_part]++;
    }
    if (LCDML.BT_checkDown()) {
      eepromSettings.ip[g_ip_selected_part]--;
    }

    if (LCDML.BT_checkEnter()) {
      g_ip_selected_part++;
    }
    if (g_ip_selected_part > 3) {
      esettings.saveSettings();
      LCDML.FUNC_goBackToMenu();
    }

    for (int i = 0; i < 16; ++i) {
      buff[i] = ' ';
    }
    for (int i = 0; i < 4; ++i) {
      char str[4];

      itoa(eepromSettings.ip[i], str, 10);
      uint8_t len = strlen(str);

      if (len > 3)
        len = 3;
      uint8_t offset = 3 - len;

      for (uint8_t j = 0; j < len; ++j) {
        buff[j + offset + i * 4] = str[j];
      }

      buff[3] = '.';
      buff[7] = '.';
      buff[11] = '.';
    }

    lcd.setCursor(0, 1);
    lcd.print(buff);
    lcd.setCursor(4 * g_ip_selected_part + 2, 1);

    if (LCDML.BT_checkQuit()) {
      LCDML.FUNC_goBackToMenu();
    }
  }

  if (LCDML.FUNC_close())      // ****** STABLE END *********
  {
    lcd.noCursor();
    // you can here reset some global vars or do nothing
  }
}
void mFunc_setting_Port(uint8_t param) {

  if (LCDML.FUNC_setup())          // ****** SETUP *********
  {
    // setup function
    lcd.setCursor(0, 0);
    lcd.print(("Port:"));
    lcd.cursor();
    LCDML.FUNC_setLoopInterval(500);
  }

  if (LCDML.FUNC_loop())           // ****** LOOP *********
  {
    if (LCDML.BT_checkAny())
      g_velocity_counter++;
    else
      g_velocity_counter = 0;

    if (LCDML.BT_checkUp()) {
      eepromSettings.port += g_velocity_counter;
    }
    if (LCDML.BT_checkDown()) {
      eepromSettings.port -= g_velocity_counter;
    }
    char buff[17] = "                ";

    char str[6];

    itoa(eepromSettings.port, str, 10);
    uint8_t len = strlen(str);

    for (uint8_t j = 0; j < len; ++j) {
      buff[j] = str[j];
    }

    lcd.setCursor(0, 1);
    lcd.print(buff);
    lcd.setCursor(4 * g_ip_selected_part + 2, 1);

    if (LCDML.BT_checkQuit()) {
      LCDML.FUNC_goBackToMenu();
    }

  }

  if (LCDML.FUNC_close())      // ****** STABLE END *********
  {
    lcd.noCursor();
    g_velocity_counter = 0;
    esettings.saveSettings();
    // you can here reset some global vars or do nothing
  }
}
void mFunc_back(uint8_t param) {
  if (LCDML.FUNC_setup())          // ****** SETUP *********
  {
    // end function and go an layer back
    LCDML.FUNC_goBackToMenu(1);      // leave this function and go a layer back
  }
}

void mFunc_GenSensorInfo(char buff[15], MoistureSensor sensor) {

  char str[5];
  uint16_t cap = sensor.getCapacitance();
  itoa(cap, str, 10);
  uint8_t len = strlen(str);
  if (len > 4)
    len = 4;
  uint8_t offset = 4 - len;

  for (uint8_t i = 0; i < len; ++i) {
    buff[i + offset] = str[i];
  }

  if (sensor.getSwitch()) {
    buff[5] = ' ';
    buff[6] = 'O';
    buff[7] = 'N';
  } else {
    buff[5] = 'O';
    buff[6] = 'F';
    buff[7] = 'F';
  }

  uint16_t temperature = sensor.getTemperature();

  dtostrf(temperature / 10.0, 4, 1, str);
  len = strlen(str);
  for (int i = 0; i < len; ++i) {
    buff[i + 9] = str[i];
  }

//  buff[9] = (temperature / 100) % 10 + 48;
//  buff[10] = (temperature / 10) % 10 + 48;
//  buff[11] = '.';
//  buff[12] = temperature % 10 + 48;
}

void mFunc_screensaver(uint8_t param) {
  if (LCDML.FUNC_setup())          // ****** SETUP *********
  {

    // update lcd content

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("1:");

    lcd.setCursor(0, 1);
    lcd.print("2:");

    lcd.setCursor(0, 2);
    lcd.print("3:");

    lcd.setCursor(0, 3);
    lcd.print("4:");

    LCDML.FUNC_setLoopInterval(1000);  // starts a trigger event for the loop function every 100 millisecounds
  }

  if (LCDML.FUNC_loop()) {

    char buff[] = "             C";
    mFunc_GenSensorInfo(buff, sensor1);
    lcd.setCursor(2, 0);
    lcd.print(buff);
    mFunc_GenSensorInfo(buff, sensor2);
    lcd.setCursor(2, 1);
    lcd.print(buff);
    mFunc_GenSensorInfo(buff, sensor3);
    lcd.setCursor(2, 2);
    lcd.print(buff);
    mFunc_GenSensorInfo(buff, sensor4);
    lcd.setCursor(2, 3);
    lcd.print(buff);


    if (LCDML.BT_checkAny()) // check if any button is pressed (enter, up, down, left, right)
    {
      LCDML.FUNC_goBackToMenu();  // leave this function
    }
  }

  if (LCDML.FUNC_close()) {
    // The screensaver go to the root menu
    LCDML.MENU_goRoot();
  }
}
void mFunc_information(uint8_t param) {
  if (LCDML.FUNC_setup())          // ****** SETUP *********
  {
    // setup function
    lcd.setCursor(0, 0);
    lcd.print(("Version:"));
    lcd.setCursor(0, 1);
    lcd.print((APP_VERSION_STR));
    lcd.setCursor(0, 2);
    lcd.print(("Serial:"));
    lcd.setCursor(0, 3);

    char buff[6];
    itoa(eepromSettings.Serial, buff, 16);
    lcd.print(buff);
  }

}
void mFunc_SensorInformation(uint8_t param) {
  MoistureSensor *sensor;
  if (param == 0)
    sensor = &sensor1;
  else if (param == 1)
    sensor = &sensor2;
  else if (param == 2)
    sensor = &sensor3;
  else if (param == 3)
    sensor = &sensor4;

  if (LCDML.FUNC_setup())          // ****** SETUP *********
  {
    // update lcd content

    lcd.clear();

    LCDML.FUNC_setLoopInterval(1000);  // starts a trigger event for the loop function every 100 millisecounds
  }

  if (LCDML.FUNC_loop()) {
    char str[6];

    char buff[] = "                ";
    itoa(sensor->getAddress(), str, 16);
    uint8_t len = strlen(str);
    buff[0] = '0';
    buff[1] = 'x';
    for (int i = 0; i < len; ++i) {
      buff[i + 2] = str[i];
    }

    buff[5] = 'V';
    buff[6] = 'C';
    buff[7] = 'C';
    buff[8] = ':';
    dtostrf(sensor->getVCC() / 100.0, 4, 2, str);
    len = strlen(str);
    for (int i = 0; i < len; ++i) {
      buff[i + 9] = str[i];
    }
    lcd.setCursor(0, 0);
    lcd.print(buff);

    for (int i = 0; i < 16; ++i) {
      buff[i] = ' ';
    }

    buff[0] = 'T';
    buff[1] = 'e';
    buff[2] = 'm';
    buff[3] = 'p';
    buff[4] = ':';

    dtostrf(sensor->getTemperature() / 10.0, 4, 1, str);
    len = strlen(str);
    for (int i = 0; i < len; ++i) {
      buff[i + 5] = str[i];
    }
    buff[9] = 'C';

    if (sensor->getSwitch()) {
      buff[13] = ' ';
      buff[14] = 'O';
      buff[15] = 'N';
    } else {
      buff[13] = 'O';
      buff[14] = 'F';
      buff[15] = 'F';
    }

    lcd.setCursor(0, 1);
    lcd.print(buff);

    for (int i = 0; i < 16; ++i) {
      buff[i] = ' ';
    }

    buff[0] = 'C';
    buff[1] = 'a';
    buff[2] = 'P';
    buff[3] = ':';

    itoa(sensor->getCapacitance(), str, 10);
    len = strlen(str);

    for (uint8_t i = 0; i < len; ++i) {
      buff[i + 4] = str[i];
    }

    itoa(sensor->getCapacitanceProz(), str, 10);
    len = strlen(str);
    for (int i = 0; i < len; ++i) {
      buff[i + 9] = str[i];
    }

    buff[9 + len + 1] = '%';

    lcd.setCursor(0, 2);
    lcd.print(buff);

    if (LCDML.BT_checkAny()) // check if any button is pressed (enter, up, down, left, right)
    {
      LCDML.FUNC_goBackToMenu();  // leave this function
    }
  }
}


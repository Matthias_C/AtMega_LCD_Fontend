//
// Created by matthiasclemen on 09.04.18.
//

#include "LCDML_control.h"
#include "LCDML_menu.h"
#include "../Timer/utility/SysTime.h"
#include "../Encoder/RotEncoder.h"

// settings
unsigned long g_LCDML_DISP_press_time = 0;


// *********************************************************************
void lcdml_menu_control(void) {
  // If something must init, put in in the setup condition
  if (LCDML.BT_setup()) {
    // runs only once
  }
    if ((millis() - g_LCDML_DISP_press_time) >= 200) {
      g_LCDML_DISP_press_time = millis(); // reset press time
      if (!bitRead(PINC, PC0)) { LCDML.BT_enter(); }
      if (!bitRead(PINC, PC1)) { LCDML.BT_quit(); }
    }

  int8_t pos = rot.read();
  if (pos > 0) {
    LCDML.BT_up();
  }
  if (pos < 0) {
    LCDML.BT_down();
  }
  rot.resetPosition();

//  uint8_t but_stat = (uint8_t)(mcp.digitalRead()>>8);
//
//
//      g_LCDML_DISP_press_time = millis(); // reset press time
//
//      if (bitRead(but_stat, 2)) { LCDML.BT_enter(); }
//      if (bitRead(but_stat, 6)) { LCDML.BT_up(); }
//      if (bitRead(but_stat, 4)) { LCDML.BT_down(); }
//      if (bitRead(but_stat, 7)) { LCDML.BT_quit(); }
//      if (bitRead(but_stat, 0)) { LCDML.BT_left(); }
//      if (bitRead(but_stat, 5)) { LCDML.BT_right(); }
//    }
//  }
}
//
// Created by matthiasclemen on 09.04.18.
//

#ifndef ATMEGALCD_FRONEND_LCML_DISPLAY_MENUFUNCTIONS_H
#define ATMEGALCD_FRONEND_LCML_DISPLAY_MENUFUNCTIONS_H

#include <stdint.h>
void mFunc_setting_IP(uint8_t param);
void mFunc_back(uint8_t param);
void mFunc_screensaver(uint8_t param);
void mFunc_setting_Port(uint8_t param);
void mFunc_information(uint8_t param);
void mFunc_SensorInformation(uint8_t param);


#endif //ATMEGALCD_FRONEND_LCML_DISPLAY_MENUFUNCTIONS_H

//
// Created by matthiasclemen on 06.11.17.
// Adapted from Arduino Libary for the AtMega328
//

#include <avr/interrupt.h>
#include "SPI.h"

SPIClass SPI;

uint8_t SPIClass::initialized = 0;
uint8_t SPIClass::interruptMode = 0;
uint8_t SPIClass::interruptMask = 0;
uint8_t SPIClass::interruptSave = 0;
#ifdef SPI_TRANSACTION_MISMATCH_LED
uint8_t SPIClass::inTransactionFlag = 0;
#endif

void SPIClass::begin() {
  uint8_t sreg = SREG;
  cli(); // Protect from a scheduler and prevent transactionBegin
  if (!initialized) {

    // Set SS to high so a connected chip will be "deselected" by default
    uint8_t bit =_BV(SPI_PIN_SS);
    volatile uint8_t *reg = &SPI_PORT_SS;

    // if the SS pin is not already configured as an output
    // then set it high (to enable the internal pull-up resistor)
    if(!(*reg & bit)){
      SPI_PORT_SS |= _BV(SPI_PIN_SS);
    }

    // When the SS pin is set as OUTPUT, it can be used as
    // a general purpose output port (it doesn't influence
    // spi operations).
    SPI_DDR_SS |= _BV(SPI_PIN_SS);



    // Warning: if the SS pin ever becomes a LOW INPUT then spi
    // automatically switches to Slave, so the data direction of
    // the SS pin MUST be kept as OUTPUT.
    SPCR |= _BV(MSTR);
    SPCR |= _BV(SPE);

    // Set direction register for SCK and MOSI pin.
    // MISO pin automatically overrides to INPUT.
    // By doing this AFTER enabling spi, we avoid accidentally
    // clocking in a single bit since the lines go directly
    // from "input" to spi control.
    // http://code.google.com/p/arduino/issues/detail?id=888

    //Set SCK & MOSI Pin aas Output
    SPI_DDR_SCK |= _BV(SPI_PIN_SCK);
    SPI_DDR_MOSI |= _BV(SPI_PIN_MOSI);

  }
  initialized++; // reference count
  SREG = sreg;
}
void SPIClass::end() {

  uint8_t sreg = SREG;
  cli(); // Protect from a scheduler and prevent transactionBegin
  // Decrease the reference counter
  if (initialized)
    initialized--;
  // If there are no more references disable spi
  if (!initialized) {
    SPCR &= ~_BV(SPE);
    interruptMode = 0;
#ifdef SPI_TRANSACTION_MISMATCH_LED
    inTransactionFlag = 0;
#endif
  }
  SREG = sreg;
}
void SPIClass::beginTransaction(SPISettings settings) {
  if (interruptMode > 0) {
    uint8_t sreg = SREG;
    cli();

#ifdef SPI_AVR_EIMSK
    if (interruptMode == 1) {
        interruptSave = SPI_AVR_EIMSK;
        SPI_AVR_EIMSK &= ~interruptMask;
        SREG = sreg;
      } else
#endif
    {
      interruptSave = sreg;
    }
  }

#ifdef SPI_TRANSACTION_MISMATCH_LED
    if (inTransactionFlag) {
      pinMode(SPI_TRANSACTION_MISMATCH_LED, OUTPUT);
      digitalWrite(SPI_TRANSACTION_MISMATCH_LED, HIGH);
    }
    inTransactionFlag = 1;
#endif

  SPCR = settings.spcr;
  SPSR = settings.spsr;
}
void SPIClass::endTransaction(void) {
#ifdef SPI_TRANSACTION_MISMATCH_LED
  if (!inTransactionFlag) {
      pinMode(SPI_TRANSACTION_MISMATCH_LED, OUTPUT);
      digitalWrite(SPI_TRANSACTION_MISMATCH_LED, HIGH);
    }
    inTransactionFlag = 0;
#endif

  if (interruptMode > 0) {
#ifdef SPI_AVR_EIMSK
    uint8_t sreg = SREG;
#endif
    cli();
#ifdef SPI_AVR_EIMSK
    if (interruptMode == 1) {
        SPI_AVR_EIMSK = interruptSave;
        SREG = sreg;
      } else
#endif
    {
      SREG = interruptSave;
    }
  }
}
uint8_t SPIClass::transfer(uint8_t data) {
  SPDR = data;
  /*
   * The following NOP introduces a small delay that can prevent the wait
   * loop form iterating when running at the maximum speed. This gives
   * about 10% more speed, even if it seems counter-intuitive. At lower
   * speeds it is unnoticed.
   */
  asm volatile("nop");
  while (!(SPSR & _BV(SPIF))) ; // wait
  return SPDR;
}
uint16_t SPIClass::transfer16(uint16_t data) {
  union { uint16_t val; struct { uint8_t lsb; uint8_t msb; }; } in = {}, out = {};
  in.val = data;
  // Check for Byte Order
  if (!(SPCR & _BV(DORD))) {
    SPDR = in.msb;
    asm volatile("nop"); // See transfer(uint8_t) function
    while (!(SPSR & _BV(SPIF))) ;
    out.msb = SPDR;
    SPDR = in.lsb;
    asm volatile("nop");
    while (!(SPSR & _BV(SPIF))) ;
    out.lsb = SPDR;
  } else {
    SPDR = in.lsb;
    asm volatile("nop");
    while (!(SPSR & _BV(SPIF))) ;
    out.lsb = SPDR;
    SPDR = in.msb;
    asm volatile("nop");
    while (!(SPSR & _BV(SPIF))) ;
    out.msb = SPDR;
  }
  return out.val;
}
void SPIClass::transfer(void *buf, size_t count) {
  if (count == 0) return;
  uint8_t *p = (uint8_t *)buf;
  SPDR = *p;
  while (--count > 0) {
    uint8_t out = *(p + 1);
    while (!(SPSR & _BV(SPIF))) ;
    uint8_t in = SPDR;
    SPDR = out;
    *p++ = in;
  }
  while (!(SPSR & _BV(SPIF))) ;
  *p = SPDR;
}


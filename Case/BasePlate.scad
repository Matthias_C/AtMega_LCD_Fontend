//$fn=64;
include <arduino.scad>

difference(){
    translate([15,57,-36.5])cube([146,120,3 ],center=true);
   translate([-45,15,-38]) Mounts();
}
translate([15,-1.5,-36.5+1.5])cube([146,3,3 ],center=true);
translate([15,-1.5+6,-36.5+1.5])cube([146,3,3 ],center=true);


translate([15-71.5+1.5,-1.5,-17.5])cube([3,3,35 ],center=true);
translate([15-71.5+1.5,-1.5+6,-17.5])cube([3,3,35 ],center=true);

hull(){
    translate([15-71.5,57,-17.5-35/2])rotate([0,90,0])cube([3,120,3 ],center=true);
    translate([15-71.5,1.5,-17.5])cube([3,9,35 ],center=true);
}



translate([15-71.5-1.5+143,-1.5,-17.5])cube([3,3,35 ],center=true);
translate([15-71.5-1.5+143,-1.5+6,-17.5])cube([3,3,35 ],center=true);

hull(){
    translate([15+71.5, 57, -17.5-35/2])rotate([0,90,0])cube([3,120,3 ],center=true);
    translate([15+71.5, 1.5,-17.5])cube([3,9,35 ],center=true);
}


translate([20,30, -17.5-35/2-2]) standoffs(UNO, mountType=TAPHOLE);

translate([-8,60, -17.5-35/2-2]) rotate([0,0,90])tap();
module tap( height = 10,  topRadius = mountingHoleRadius + 1,   bottomRadius =  mountingHoleRadius + 2, holeRadius = mountingHoleRadius){
     for(i = [[  2.54, 2.54 ], [  2.54, 2.54*13 ],  [  2.54*10, 2.54 ],  [  2.54*10, 2.54 *13]] ) {
        translate(i)
            union() {
                  difference() {
                    cylinder(r1 = bottomRadius, r2 = topRadius, h = height, $fn=32);
                    cylinder(r =  holeRadius, h = height * 4, center = true, $fn=32);
                   
                  }
                
        } 
    }
}


module Mounts(){
     for(i = [[  0, 0 ], [  0, 90 ],  [ 120, 0 ],  [  120, 90]] ) {
        translate(i)
         union(){
                    cylinder(d = 3, h = 3, $fn=32);

                 translate([0, 0,1.5])   cylinder(d = 5.5, h = 1.5, $fn=6);
                   
                  }
                
         
    }
}

%rotate([90,0,0])FrontPanel();

module FrontPanel(){
    difference(){
    union(){
        translate([15,0,-1.5])cube([140,70,3 ],center=true);
        
        translate([92/2,55/2,-10])cylinder(d=6,h=10);
        translate([92/2,-55/2,-10])cylinder(d=6,h=10);
        translate([-92/2,55/2,-10])cylinder(d=6,h=10);
        translate([-92/2,-55/2,-10])cylinder(d=6,h=10);
        
        translate([65,15,0]){
        
            translate([18/2,21/2,-5.5])cylinder(d=6,h=5.5);
            translate([18/2,-21/2,-5.5])cylinder(d=6,h=5.5);
            translate([-18/2,21/2,-5.5])cylinder(d=6,h=5.5);
            translate([-18/2,-21/2,-5.5])cylinder(d=6,h=5.5);
        }
        
    }
    translate([65,15,-6]){
        cylinder(d=7,h=6);
        };
        
        translate([65,15,0]){
        
            translate([18/2,21/2,-5.5])cylinder(d=2,h=4.5);
            translate([18/2,-21/2,-5.5])cylinder(d=2,h=4.5);
            translate([-18/2,21/2,-5.5])cylinder(d=2,h=4.5);
            translate([-18/2,-21/2,-5.5])cylinder(d=2,h=4.5);
        }
}
    }


module RotatyEncoder(){
    
    cylinder(d=7,h=6);
}

module LCD(){
LCD_Width = 97;
LCD_Height = 40;
LCD_Depth = 10;
 translate([0,0,-5])cube([LCD_Width,LCD_Height,LCD_Depth ],center=true);
    
    
 translate([92/2,55/2,-10])cylinder(d=2,h=8);;
 translate([92/2,-55/2,-10])cylinder(d=2,h=8);;
 translate([-92/2,55/2,-10])cylinder(d=2,h=8);;
 translate([-92/2,-55/2,-10])cylinder(d=2,h=8);;
}
    
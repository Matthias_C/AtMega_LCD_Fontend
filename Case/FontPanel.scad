$fn=64;
       translate([65,15,-4.5]) buttonCap();
difference(){
    union(){
        translate([15,0,-1.5])cube([140,70,3 ],center=true);
        
        translate([92/2,55/2,-10])cylinder(d=6,h=10);
        translate([92/2,-55/2,-10])cylinder(d=6,h=10);
        translate([-92/2,55/2,-10])cylinder(d=6,h=10);
        translate([-92/2,-55/2,-10])cylinder(d=6,h=10);
        
        translate([65,15,0]){
        
            translate([18/2,21/2,-5.5])cylinder(d=6,h=5.5);
            translate([18/2,-21/2,-5.5])cylinder(d=6,h=5.5);
            translate([-18/2,21/2,-5.5])cylinder(d=6,h=5.5);
            translate([-18/2,-21/2,-5.5])cylinder(d=6,h=5.5);
        }
        
    }
    LCD();
    translate([65,-15,-6])RotatyEncoder();
    translate([65,15,-6]){
        cylinder(d=7,h=6);
        };
        
        translate([65,15,0]){
        
            translate([18/2,21/2,-5.5])cylinder(d=2,h=4.5);
            translate([18/2,-21/2,-5.5])cylinder(d=2,h=4.5);
            translate([-18/2,21/2,-5.5])cylinder(d=2,h=4.5);
            translate([-18/2,-21/2,-5.5])cylinder(d=2,h=4.5);
        }
}
translate([65,-15,60]) rotCap();
module rotCap() {
    difference(){
        
        
    cylinder(d=15,h=13);
    cylinder(d=6,h=10);
    cylinder(d=10,h=4);
        }
    }

module buttonCap() {
    union(){
        
        
    cylinder(d=6.5,h=5);
    cylinder(d=8,h=1);
        }
    }

module RotatyEncoder(){
    
    cylinder(d=7,h=6);
}

module LCD(){
LCD_Width = 97;
LCD_Height = 40;
LCD_Depth = 10;
 translate([0,0,-5])cube([LCD_Width,LCD_Height,LCD_Depth ],center=true);
    
    
 translate([92/2,55/2,-10])cylinder(d=2,h=8);;
 translate([92/2,-55/2,-10])cylinder(d=2,h=8);;
 translate([-92/2,55/2,-10])cylinder(d=2,h=8);;
 translate([-92/2,-55/2,-10])cylinder(d=2,h=8);;
}
    
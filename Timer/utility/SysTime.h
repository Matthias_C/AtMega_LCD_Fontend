//
// Created by matthiasclemen on 04.04.18.
//

#ifndef ATMEGALCD_FRONEND_SYSTIME_H
#define ATMEGALCD_FRONEND_SYSTIME_H

#include <inttypes.h>
#ifdef __cplusplus
extern "C"{
#endif

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

void init();

unsigned long millis(void);
unsigned long micros(void);

#ifdef __cplusplus
} // extern "C"
#endif

#endif //ATMEGALCD_FRONEND_SYSTIME_H

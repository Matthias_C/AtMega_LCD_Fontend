//
// Created by matthiasclemen on 26.04.18.
//

#ifndef ATMEGALCD_FRONEND_ROTENCODER_H
#define ATMEGALCD_FRONEND_ROTENCODER_H

#include <stdint.h>
#include "../Interrupt/Interrupt.h"
class RotEncoder {
 public:
  RotEncoder();
  void resetPosition();
  int8_t read();



 private:

  class extInterrupt1 : public interrupt {
    static RotEncoder *ownerTimer;
    static void serviceRoutine() __asm__("__vector_2") __attribute__((__signal__, __used__, __externally_visible__));

   public:
    static void record(RotEncoder *ownerTimer);
  };

  friend extInterrupt1;

  uint8_t state;
  uint8_t oldState;
  int8_t position;
  int i;
};

extern RotEncoder rot;

#endif //ATMEGALCD_FRONEND_ROTENCODER_H

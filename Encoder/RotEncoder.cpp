//
// Created by matthiasclemen on 26.04.18.
//

#include <avr/io.h>
#include <avr/interrupt.h>
#include "RotEncoder.h"
#include "../Globals.h"

RotEncoder *RotEncoder::extInterrupt1::ownerTimer = 0;

RotEncoder::RotEncoder() : i(0), state(0), position(0) {
  extInterrupt1::record(this);
  bitWrite(EICRA, ISC11, 1);
  bitWrite(EICRA, ISC10, 0);
  bitWrite(EIMSK, INT1, 1);


  sei();
}

int8_t RotEncoder::read() {

  return position;
}

void RotEncoder::resetPosition() {
  position = 0;
}

void RotEncoder::extInterrupt1::serviceRoutine() {

  if (ownerTimer != 0) {
    uint8_t val = bitRead(PINC, PC2);
    if (val) {
      ownerTimer->position--;
    } else
      ownerTimer->position++;


//    uint8_t p1val = bitRead(PIND,PD3);
//    uint8_t p2val = bitRead(PINC,PC2);
//    uint8_t state = ownerTimer->state & 3;
//    if (p1val) state |= 4;
//    if (p2val) state |= 8;
//    ownerTimer->state = (state >> 2);
//    switch (state) {
//      case 1: case 7: case 8: case 14:
//        ownerTimer->position++;
//        return;
//      case 2: case 4: case 11: case 13:
//        ownerTimer->position--;
//        return;
//      case 3: case 12:
//        ownerTimer->position += 2;
//        return;
//      case 6: case 9:
//        ownerTimer->position -= 2;
//        return;
//    }
  }
}

void RotEncoder::extInterrupt1::record(RotEncoder *t) {
  ownerTimer = t;
}

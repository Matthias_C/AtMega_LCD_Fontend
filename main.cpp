

#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "SPI/SPI.h"
#include "MCP23S17/MCP23S17.h"
#include "Globals.h"
#include "Timer/utility/SysTime.h"
#include "MenuLib/LCDML_menu.h"
#include "Encoder/RotEncoder.h"
#include "etherShield/ETHER_28J60.h"

SPIClass spi;
MCP mcp = MCP(0, PB1, &DDRB, &PORTB);

MoistureSensor sensor1 = MoistureSensor(0x71);
MoistureSensor sensor2 = MoistureSensor(0x72);
MoistureSensor sensor3 = MoistureSensor(0x73);
MoistureSensor sensor4 = MoistureSensor(0x74);
RotEncoder rot;

Wire wire;
LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 20, 4);

Settings esettings;

EepromSettings eepromSettings;

ETHER_28J60 e;

//void printSensor(char i, ETHER_28J60 ether, MoistureSensor sensor) {
//  char buff[6];
//  ether.print("\"sensor");
//  ether.print(i);
//  ether.print("\": {\"Temperature\":");
//  itoa(sensor.getTemperature(), buff, 10);
//  ether.print(buff);
//  ether.print(", \"Capacity\":");
//  itoa(sensor.getCapacitance(), buff, 10);
//  ether.print(buff);
//  ether.print("}");
//}


static uint8_t ip[4] = {192, 168, 88, 15};
static uint8_t mac[6] = {0x54, 0x55, 0x58, 0x10, 0x00, 0x24};
static uint16_t port = 80;
int main() {
  //init SysTimea
  init();

  esettings.restoreSettings();

  if (eepromSettings.Serial < 1 || eepromSettings.Serial >= 0xFFFF) {
    eepromSettings.IPModeDHCP = false;

    eepromSettings.Serial = 1;
    eepromSettings.mac[0] = 0x54;
    eepromSettings.mac[1] = 0x55;
    eepromSettings.mac[2] = 0x58;
    eepromSettings.mac[3] = 0x10;
    eepromSettings.mac[4] = (uint8_t) (eepromSettings.Serial >> 0x8);
    eepromSettings.mac[5] = (uint8_t) eepromSettings.Serial;

    eepromSettings.ip[0] = 192;
    eepromSettings.ip[1] = 168;
    eepromSettings.ip[2] = 88;
    eepromSettings.ip[3] = 55;

    eepromSettings.port = 80;
    esettings.saveSettings();
  }

  eepromSettings.Serial = 1;
  eepromSettings.mac[0] = 0x54;
  eepromSettings.mac[1] = 0x55;
  eepromSettings.mac[2] = 0x58;
  eepromSettings.mac[3] = 0x10;
  eepromSettings.mac[4] = (uint8_t) (eepromSettings.Serial >> 0x8);
  eepromSettings.mac[5] = (uint8_t) eepromSettings.Serial;

  eepromSettings.ip[0] = 192;
  eepromSettings.ip[1] = 168;
  eepromSettings.ip[2] = 88;
  eepromSettings.ip[3] = 55;

  eepromSettings.port = 80;


  cli();

  spi.begin();

  wire.begin();

  e.setup(mac, ip, port);
  // ETHER_28J60 ether = ETHER_28J60(PB2, &DDRB, &PORTB);
  // ether.setup(eepromSettings.mac, eepromSettings.ip, eepromSettings.port);
  sei();

  mcp.begin(spi);

  lcd.begin();
  lcd.noBlink();
  lcd.noCursor();

  bitWrite(DDRD, PD4, OUTPUT);

  bitWrite(PORTD, PD4, 1);
  bitWrite(PORTD, PD4, 0);

  LCDML_menu menu;
  menu.begin();

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

  char c = 1;

  while (1) {
    char *params;

//    if (params = e.serviceRequest()) {
//      e.print("<H1>Web Remote</H1>");
//      if (strcmp(params, "?cmd=on") == 0) {
//        e.print("<A HREF='?cmd=off'>Turn off</A>");
//      } else if (strcmp(params, "?cmd=off") == 0) // Modified -- 2011 12 15 # Ben Schueler
//      {
//        e.print("<A HREF='?cmd=on'>Turn on</A>");
//      }
//      e.respond();
//    }
//    if (params = ether.serviceRequest()) {
//      ether.print("{");
////      printSensor('1', ether, sensor1);
////      ether.print(",");
////      printSensor('2', ether, sensor2);
////      ether.print(",");
////      printSensor('3', ether, sensor3);
////      ether.print(",");
////      printSensor('4', ether, sensor4);
//      ether.print("}");
//
//      ether.respond();
//    }

    LCDML.loop();
  }
#pragma clang diagnostic pop

}

// *********************************************************************
// check some errors - do not change here anything
// *********************************************************************
# if(_LCDML_DISP_rows > _LCDML_DISP_cfg_max_rows)
# error change value of _LCDML_DISP_cfg_max_rows in LCDMenuLib2.h
# endif
//
// Created by matthiasclemen on 27.03.18.
//

#include <util/delay.h>
#include "MoistureSensor.h"

#include "../Globals.h"

void MoistureSensor::begin(bool wait) {
  restart();
  if (wait) {
    _delay_ms(1000);
  }
}

uint16_t MoistureSensor::getCapacitanceProz() {
  return readI2CRegister_U16(sensorAddress, SENSOR_GET_CAPACITANCE_PROZ);
}

uint16_t MoistureSensor::getCapacitance() {
  return readI2CRegister_U16(sensorAddress, SENSOR_GET_CAPACITANCE);
}

bool MoistureSensor::setCalibrateHigh(unsigned int value) {
  writeI2CRegister_8(sensorAddress, SENSOR_SET_CALIBRATE_HIGH);
  return static_cast<bool>(readI2CRegister_U16(sensorAddress, SENSOR_GET_CALIBRATE_HIGH));
}

unsigned int MoistureSensor::getCalibrateHigh() {
  return readI2CRegister_U16(sensorAddress, SENSOR_GET_CALIBRATE_HIGH);
}

bool MoistureSensor::setCalibrateLow(unsigned int value) {
  writeI2CRegister_8(sensorAddress, SENSOR_SET_CALIBRATE_LOW);
  return static_cast<bool>(readI2CRegister_U16(sensorAddress, SENSOR_GET_CALIBRATE_LOW));
}

unsigned int MoistureSensor::getCalibrateLow() {
  return readI2CRegister_U16(sensorAddress, SENSOR_GET_CALIBRATE_LOW);
}

uint16_t MoistureSensor::getVCC() {
  return readI2CRegister_U16(sensorAddress, SENSOR_GET_VCC);
}

int MoistureSensor::getTemperature() {
  return readI2CRegister_16(sensorAddress, SENSOR_GET_TEMPERATURE);
}

bool MoistureSensor::setAlarmOn(uint8_t alarm) {
  writeI2CRegister_8(sensorAddress, SENSOR_SET_ALARM_ON, alarm);
  return (readI2CRegister_8(sensorAddress, SENSOR_GET_ALARM_ON) == alarm);
}

uint8_t MoistureSensor::getAlarmOn() {
  return readI2CRegister_8(sensorAddress, SENSOR_GET_ALARM_ON);
}

bool MoistureSensor::setAlarmOff(uint8_t alarm) {
  writeI2CRegister_8(sensorAddress, SENSOR_SET_ALARM_OFF, alarm);
  return (readI2CRegister_8(sensorAddress, SENSOR_GET_ALARM_OFF) == alarm);
}

uint8_t MoistureSensor::getAlarmOff() {
  return readI2CRegister_8(sensorAddress, SENSOR_GET_ALARM_OFF);
}

bool MoistureSensor::setAddress(uint8_t addr, bool reset) {
  writeI2CRegister_8(sensorAddress, SENSOR_SET_ADDRESS, addr);
  if (reset) {
    restart();
    _delay_ms(1000);
  }
  sensorAddress = addr;
  return (readI2CRegister_8(sensorAddress, SENSOR_GET_ADDRESS) == addr);
}

void MoistureSensor::changeAddress(uint8_t addr, bool wait) {
  sensorAddress = addr;
  begin(wait);
}

uint8_t MoistureSensor::getAddress() {
  return sensorAddress;
}

void MoistureSensor::restart() {
  writeI2CRegister_8(sensorAddress, SENSOR_RESET);
}

uint8_t MoistureSensor::getVersion() {
  return readI2CRegister_8(sensorAddress, SENSOR_GET_VERSION);
}

uint8_t MoistureSensor::getSwitch() {
  return readI2CRegister_8(sensorAddress, SENSOR_GET_SW);
}

void MoistureSensor::writeI2CRegister_8(uint8_t addr, uint8_t value) {
  wire.beginTransmission(addr);
  wire.write(value);
  wire.endTransmission();
}

void MoistureSensor::writeI2CRegister_8(uint8_t addr, uint8_t reg, uint8_t value) {
  wire.beginTransmission(addr);
  wire.write(reg);
  wire.write(value);
  wire.endTransmission();
}

uint16_t MoistureSensor::readI2CRegister_U16(uint8_t addr, uint8_t reg) {
  wire.beginTransmission(addr);
  wire.write(reg);
  uint8_t ret = wire.endTransmission();
  if (ret == 0) {
    _delay_ms(20);
    wire.requestFrom(addr, 2, 0);
    uint16_t t = wire.read() << 8;
    t = t | wire.read();
    return t;
  }
  return 0;
}

int MoistureSensor::readI2CRegister_16(uint8_t addr, uint8_t reg) {
  wire.beginTransmission(addr);
  wire.write(reg);
  uint8_t ret = wire.endTransmission();
  if (ret == 0) {
    _delay_ms(20);
    wire.requestFrom(addr, 2, 0);
    int16_t t = wire.read() << 8;
    t = t | wire.read();
    return t;
  }
  return 0;
}
uint8_t MoistureSensor::readI2CRegister_8(uint8_t addr, uint8_t reg) {
  wire.beginTransmission(addr);
  wire.write(reg);
  uint8_t ret = wire.endTransmission();
  if (ret == 0) {
    _delay_ms(20);
    wire.requestFrom(addr, 1, 0);
    return wire.read();
  }
  return 0;
}

//
// Created by matthiasclemen on 27.03.18.
//

#ifndef ATMEGALCD_FRONEND_MOISTURESENSOR_H
#define ATMEGALCD_FRONEND_MOISTURESENSOR_H

#include <stdint-gcc.h>

#define SENSOR_DEFAULT_ADDR                    0x71

//Sensor Register Addresses
#define SENSOR_GET_CAPACITANCE_PROZ        0x00 // (r) 	2 bytes
#define SENSOR_GET_CAPACITANCE                0x01 // (r) 	2 bytes

#define SENSOR_SET_ADDRESS                        0x02 //	(w) 	1 byte
#define SENSOR_GET_ADDRESS                        0x03 // (r) 	1 byte

#define SENSOR_SET_CALIBRATE_HIGH            0x04 //	(w) 	n/a
#define SENSOR_GET_CALIBRATE_HIGH            0x05 //	(w) 	n/a

#define SENSOR_SET_CALIBRATE_LOW            0x06 //	(r) 	2 bytes
#define SENSOR_GET_CALIBRATE_LOW            0x07 //	(r) 	2 bytes

#define SENSOR_GET_TEMPERATURE                0x08 //	(r) 	2 bytes

#define SENSOR_SET_ALARM_ON                        0x09 //	(w) 	1 byte
#define SENSOR_GET_ALARM_ON                        0x0A //	(r) 	1 byte
#define SENSOR_SET_ALARM_OFF                    0x0B //	(w) 	1 byte
#define SENSOR_GET_ALARM_OFF                    0x0C //	(r) 	1 byte

#define SENSOR_RESET                                    0x0D //	(w) 	n/a
#define SENSOR_GET_VERSION                        0x0E //	(r) 	1 bytes

#define SENSOR_GET_VCC                                0x0F // (r) 	2 bytes

#define SENSOR_GET_SW                                    0x10 // (r) 	2 bytes

class MoistureSensor {
 public:
  MoistureSensor(uint8_t addr) : sensorAddress(addr) {}

  void begin(bool wait = false);
  uint16_t getCapacitance();
  uint16_t getCapacitanceProz();

  uint16_t getVCC();

  bool setCalibrateHigh(uint16_t = 0);
  uint16_t getCalibrateHigh();

  bool setCalibrateLow(uint16_t = 0);
  uint16_t getCalibrateLow();

  int16_t getTemperature();

  bool setAddress(uint8_t addr, bool reset);
  uint8_t getAddress();
  void changeAddress(uint8_t addr, bool wait = false);

  bool setAlarmOn(uint8_t alarm);
  uint8_t getAlarmOn();
  bool setAlarmOff(uint8_t alarm);
  uint8_t getAlarmOff();

  void restart();
  uint8_t getVersion();

  uint8_t getSwitch();

 private:
  uint8_t sensorAddress;

  void writeI2CRegister_8(uint8_t addr, uint8_t value);
  void writeI2CRegister_8(uint8_t addr, uint8_t reg, uint8_t value);
  uint8_t readI2CRegister_8(uint8_t addr, uint8_t reg);

  uint16_t readI2CRegister_U16(uint8_t addr, uint8_t reg);
  int readI2CRegister_16(uint8_t addr, uint8_t reg);
};

#endif //ATMEGALCD_FRONEND_MOISTURESENSOR_H
